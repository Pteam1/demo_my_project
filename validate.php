
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"

"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">

<head>

	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

	<title>Admintasia 2.3</title>

	<script type="text/javascript" src="js/jquery-1.6.2.js"></script>

	<script type="text/javascript" src="js/ui/ui.core.js"></script>

	<script type="text/javascript" src="js/ui/ui.widget.js"></script>

	<script type="text/javascript" src="js/ui/ui.mouse.js"></script>

	<script type="text/javascript" src="js/superfish.js"></script>

	<script type="text/javascript" src="js/live_search.js"></script>

	<script type="text/javascript" src="js/tooltip.js"></script>

	<script type="text/javascript" src="js/cookie.js"></script>

	<script type="text/javascript" src="js/ui/ui.sortable.js"></script>

	<script type="text/javascript" src="js/ui/ui.draggable.js"></script>

	<script type="text/javascript" src="js/ui/ui.resizable.js"></script>

	<script type="text/javascript" src="js/ui/ui.position.js"></script>

	<script type="text/javascript" src="js/ui/ui.button.js"></script>

	<script type="text/javascript" src="js/ui/ui.dialog.js"></script>

	<script type="text/javascript" src="js/custom.js"></script>

	

	<link href="css/ui/ui.base.css" rel="stylesheet" media="all" />

	<link href="css/themes/black_rose/ui.css" rel="stylesheet" title="style" media="all" />



	<!--[if IE 6]>

	<link href="css/ie6.css" rel="stylesheet" media="all" />

	

	<script src="js/pngfix.js"></script>

	<script>

	  /* Fix IE6 Transparent PNG */

	  DD_belatedPNG.fix('.logo, ul#dashboard-buttons li a, .response-msg, #search-bar input');



	</script>

	<![endif]-->

</head>

<body>



	<div id="page_wrapper">

		<div id="page-header">

			<div id="page-header-wrapper">

				<div id="top">

					<a href="dashboard.php" class="logo" title="Admintasia 2.3">Admintasia 2.3</a>

					<div class="welcome">

						<span class="note">Welcome, <a href="#" title="Welcome, Horia Simon">Horia Simon</a></span>

						<a class="btn ui-state-default ui-corner-all" href="#">

							<span class="ui-icon ui-icon-wrench"></span>

							Settings

						</a>

						<a class="btn ui-state-default ui-corner-all" href="#">

							<span class="ui-icon ui-icon-person"></span>

							My account

						</a>

						<a class="btn ui-state-default ui-corner-all" href="#">

							<span class="ui-icon ui-icon-power"></span>

							Logout

						</a>						

					</div>

				</div>

				<ul id="navigation">

					<li>

						<a href="dashboard.php" class="sf-with-ul">Dashboard</a>

						<ul>

							<li><a href="dashboard.php">Administration</a></li>

							<li>

								<a href="forms.php">Forms</a>

								<ul>

									<li><a href="validate.php">Form validation</a></li>

									<li><a href="table_modal.php"><b>Add to table modal</b></a></li>

									<li><a href="editinplace.php"><b>Edit in Place</b></a></li>

									<li><a href="tinymce.php"><b>WYSIWYG Editor</b></a></li>

								</ul>

							</li>

							<li>

								<a href="tables.php">Tables</a>

								<ul>

									<li><a href="tables.php">Sortable Tables</a></li>

									<li><a href="flexigrid.php"><b>FlexiGrid</b></a></li>

								</ul>

							</li>

							<li>

								<a href="#">Widgets</a>

								<ul>

									<li><a href="accordion.php">Accordion</a></li>

									<li><a href="flexigrid.php"><b>FlexiGrid</b></a></li>

									<li><a href="editinplace.php"><b>Edit in Place</b></a></li>

									<li><a href="tinymce.php"><b>WYSIWYG Editor</b></a></li>

									<li><a href="charts.php"><b>Charts</b></a></li>

									<li><a href="tabs.php">Tabs</a></li>

									<li><a href="slider.php">Slider</a></li>

									<li><a href="datepicker.php">Datepicker</a></li>

									<li><a href="progress.php">Progress Bar</a></li>

									<li><a href="dialog.php">Dialogs and Modals</a></li>

									<li><a href="overlays.php">Overlays</a></li>

									<li><a href="photo_manager.php">Photo Manager</a></li>

									<li><a href="file_browser.php">File Browser</a></li>

								</ul>

							</li>

							<li><a href="msg.php">Response Messages</a></li>

							<li><a href="icons.php">Icons</a></li>

							<li><a href="index.php">Login Page</a></li>

							<li><a href="icons.php">Buttons and Elements</a></li>

						</ul>

					</li>

					<li>

						<a href="#" class="sf-with-ul">Unlimited Levels</a>

						<ul>

							<li>

								<a href="#" class="sf-with-ul">Menu item 1</a>

								<ul>

									<li><a href="#">Subitem 1</a></li>

									<li><a href="#">Subitem 2</a></li>

								</ul>

							</li>

							<li>

								<a href="#">Menu item 2</a>

							</li>

							<li>

								<a href="#">Menu item 3</a>

							</li>

							<li>

								<a href="#" class="sf-with-ul">Menu item 4</a>

								<ul>

									<li><a href="#">Subitem 1</a></li>

									<li>

										<a href="#" class="sf-with-ul">Subitem 2</a>

										<ul>

											<li><a href="#">Subitem 1</a></li>

											<li>

												<a href="#" class="sf-with-ul">Subitem 2</a>

												<ul>

													<li><a href="#">Subitem 1</a></li>

													<li>

														<a href="#">Subitem 2</a>

													</li>

												</ul>

											</li>

										</ul>

									</li>

								</ul>

							</li>

							<li>

								<a href="#" class="sf-with-ul">Menu item 5</a>

								<ul>

									<li><a href="#">Subitem 1</a></li>

									<li><a href="#">Subitem 2</a></li>

								</ul>

							</li>

							<li>

								<a href="#">Menu item 6</a>

							</li>

							<li>

								<a href="#">Menu item 7</a>

							</li>

						</ul>

					</li>

					<li><a href="gallery.php">Photo Gallery</a></li>

					<li>

						<a href="#" class="sf-with-ul">Layout Options</a>

						<ul>

							<li>

								<a href="three-columns-layout.php">Three columns</a>

							</li>

							<li>

								<a href="two-column-layout.php">Two columns</a>

							</li>

							<li>

								<a href="no-rounded.php">No rounded corners</a>

							</li>

							<li>

								<a href="content_boxes.php">Available content boxes</a>

							</li>

						</ul>

					</li>

					<li>

						<a href="#" class="sf-with-ul">Theme Options</a>

						<ul>

							<li>

								<a href="page_left_sidebar.php">Page with left sidebar</a>

							</li>

							<li>

								<a href="page_dynamic_sidebar.php">Page with dynamic sidebar</a>

							</li>

							<li>

								<a href="#">Avaiable Themes</a>

								<ul id="style-switcher">

									<li>

										<a class="set_theme" id="black_rose" href="#" title="Black Rose Theme">Black Rose Theme</a>

									</li>

									<li>

										<a class="set_theme" id="gray_standard" href="#" title="Gray Standard Theme">Gray Standard Theme</a>

									</li>

									<li>

										<a class="set_theme" id="gray_lightness" href="#" title="Gray Lightness Theme">Gray Lightness Theme</a>

									</li>

									<li>

										<a class="set_theme" id="apple_pie" href="#" title="Apple Pie Theme">Apple Pie Theme</a>

									</li>

									<li>

										<a class="set_theme" id="blueberry" href="#" title="Blueberry Theme">Blueberry Theme</a>

									</li>
									<li>

									<a class="set_theme" id="blue_sky" href="#" title="BLUE-light blue NAVIGATION BAR Theme">BlueSky Theme</a> 

								</li>

								<li>

									<a class="set_theme" id="salmon" href="#" title="White With Blue Theme">Salmon  Theme</a>

								</li>

								<li>

									<a class="set_theme" id="turquoise" href="#" title="White With Gray Theme">Turquoise Theme</a>

								</li>


								</ul>

							</li>

							<li>

								<a href="#"><i>Dummy Link</i></a>

							</li>

						</ul>

					</li>

					<li>

						<a href="#" class="sf-with-ul">Widgets</a>

						<ul>

							<li><a href="accordion.php">Accordion</a></li>

							<li><a href="flexigrid.php"><b>FlexiGrid</b></a></li>

							<li><a href="editinplace.php"><b>Edit in Place</b></a></li>

							<li><a href="tinymce.php"><b>WYSIWYG Editor</b></a></li>

							<li><a href="charts.php"><b>Charts</b></a></li>

							<li><a href="tabs.php">Tabs</a></li>

							<li><a href="slider.php">Slider</a></li>

							<li><a href="datepicker.php">Datepicker</a></li>

							<li><a href="progress.php">Progress Bar</a></li>

							<li><a href="dialog.php">Dialogs and Modals</a></li>

							<li><a href="overlays.php">Overlays</a></li>

							<li><a href="photo_manager.php">Photo Manager</a></li>

							<li><a href="file_browser.php">File Browser</a></li>

						</ul>

					</li>

				</ul>

				<div id="search-bar">

					<form method="post" action="http://www.google.com/">

						<input type="text" name="q" value="live search demo" />

					</form>

				</div>

			</div>

		</div>


<script type="text/javascript" src="js/validate.js"></script>



	<script type="text/javascript">

$().ready(function() {

	// validate the comment form when it is submitted

	$("#commentForm").validate();

	

	// validate signup form on keyup and submit

	$("#signupForm").validate({

		rules: {

			firstname: "required",

			lastname: "required",

			username: {

				required: true,

				minlength: 2

			},

			password: {

				required: true,

				minlength: 5

			},

			confirm_password: {

				required: true,

				minlength: 5,

				equalTo: "#password"

			},

			email: {

				required: true,

				email: true

			},

			topic: {

				required: "#newsletter:checked",

				minlength: 2

			},

		messages: {

			firstname: "Please enter your firstname",

			lastname: "Please enter your lastname",

			username: {

				required: "Please enter a username",

				minlength: "Your username must consist of at least 2 characters"

			},

			password: {

				required: "Please provide a password",

				minlength: "Your password must be at least 5 characters long"

			},

			confirm_password: {

				required: "Please provide a password",

				minlength: "Your password must be at least 5 characters long",

				equalTo: "Please enter the same password as above"

			},

			email: "Please enter a valid email address",

			agree: "Please accept our policy"

		}

		}





});

});



	</script>

		<div id="sub-nav"><div class="page-title">

			<h1>Form validation example</h1>

			<span><a href="#" title="Dashboard">Dashboard</a> > <a href="#" title="Forms">Forms</a> > Form Validation</span>

		</div>

		<div id="top-buttons">

			<a id="dialog_link" class="btn ui-state-default ui-corner-all" href="#">

				<span class="ui-icon ui-icon-newwin"></span>

				Dialog Window

			</a>
     <ul class="drop-down">
				<li>

								<a class="btn ui-state-default ui-corner-all" id="drop" href="#drop_down">
					
									<span class="ui-icon ui-icon-carat-2-n-s"></span>
					
									DropDown Menu
					
								</a>
								
								<ul  class="drop-down-container box ui-widget ui-widget-content .ui-corner-tl .ui-corner-tr">
								        <li><a href="#" class="btn ui-state-default full-link ui-corner-all set_theme">DropDown Menu-1</a></li>

										<li><a href="#" class="btn ui-state-default full-link ui-corner-all set_theme">DropDown Menu-2</a></li>
					
										<li><a href="#" class="btn ui-state-default full-link ui-corner-all set_theme">DropDown Menu-3</a></li>
					
										<li><a href="#" class="btn ui-state-default full-link ui-corner-all set_theme">DropDown Menu-4</a></li>
					
										<li><a href="#" class="btn ui-state-default full-link ui-corner-all set_theme">DropDown Menu-5</a></li>
								</ul>
								</li>
								</ul>

			<div id="drop_down" class="hidden">

				<ul>

					<li><a href="#">Google</a></li>

					<li><a href="#">Yahoo</a></li>

					<li><a href="#">MSN</a></li>

					<li><a href="#">Ask</a></li>

					<li><a href="#">AOL</a></li>

				</ul>

			</div>

			<a id="modal_confirmation_link" class="btn ui-state-default ui-corner-all" href="#">

				<span class="ui-icon ui-icon-grip-dotted-horizontal"></span>

				Modal Confirmation

			</a>

		</div>

			<div id="dialog" title="Dialog Title">

				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>

			</div>

			<div id="modal_confirmation" title="An example modal title ?">

				<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>

			</div>

</div>

		<div id="page-layout"><div id="page-content">

			<div id="page-content-wrapper">

				<div class="inner-page-title">

					<h2>Validating a complete form</h2>

					<span>Lorem ipsum dolor sic amet dixit tu</span>

				</div>

				<div class="column-content-box">

					<div class="content-box content-box-header ui-corner-all">

						<div class="ui-state-default ui-corner-top ui-box-header">

							<span class="ui-icon float-left ui-icon-notice"></span>

							Validating a form

						</div>

						<div class="content-box-wrapper">

							<form class="forms" id="signupForm" method="get" action="">

								<fieldset>

									<ul>

										<li>

											<label class="desc" for="firstname">Firstname</label>

											<div><input id="firstname" class="field text full" name="firstname" /></div>

										</li>

										<li>

											<label class="desc" for="lastname">Lastname</label>

								

											<div><input id="lastname" class="field text full" name="lastname" /></div>

										</li>

										<li>

											<label class="desc" for="username">Username</label>

											<div><input id="username" class="field text full" name="username" /></div>

										</li>

										<li>

											<label class="desc" for="password">Password</label>

								

											<div><input id="password" class="field text full" name="password" type="password" /></div>

										</li>

										<li>

											<label class="desc" for="confirm_password">Confirm password</label>

											<div><input id="confirm_password" class="field text full" name="confirm_password" type="password" /></div>

										</li>

										<li>

											<label class="desc" for="email">Email</label>

											<div><input id="email" class="field text full" name="email" /></div>

										</li>

								

										<li>

											<input class="submit" type="submit" value="Submit"/>

										</li>

									</ul>

								</fieldset>

							</form>

						</div>

					</div>

				</div>				

				<div class="clearfix"></div>

						<div id="sidebar">

			<div class="sidebar-content">

				<a id="close_sidebar" class="btn ui-state-default full-link ui-corner-all" >

					<span class="ui-icon ui-icon-circle-arrow-e"></span>

					Close Sidebar

				</a>

				<a id="open_sidebar" class="btn tooltip ui-state-default full-link icon-only ui-corner-all" title="Open Sidebar" ><span class="ui-icon ui-icon-circle-arrow-w"></span></a>

				<div class="hide_sidebar">

					<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">

						<div class="portlet-header ui-widget-header">Theme Switcher<span class="ui-icon ui-icon-circle-arrow-s"></span></div>

						<div class="portlet-content">

							<ul id="style-switcher" class="side-menu">

								<li>

									<a class="set_theme" id="black_rose" href="#" title="Black Rose Theme">Black Rose Theme</a>

								</li>

								<li>

									<a class="set_theme" id="gray_standard" href="#" title="Gray Standard Theme">Gray Standard Theme</a>

								</li>

								<li>

									<a class="set_theme" id="gray_lightness" href="#" title="Gray Lightness Theme">Gray Lightness Theme</a>

								</li>

								<li>

									<a class="set_theme" id="apple_pie" href="#" title="Apple Pie Theme">Apple Pie Theme</a>

								</li>

								<li>

									<a class="set_theme" id="blueberry" href="#" title="Blueberry Theme">Blueberry Theme</a>

								</li>
								<li>
									<a class="set_theme" id="blue_sky" href="#" title="BlueSky Theme">BlueSky Theme</a> 																	     							</li>							
								<li> 
									<a class="set_theme" id="salmon" href="#" title="Salmon  Theme">Salmon  Theme</a>
								</li>
								<li>
									<a class="set_theme" id="turquoise" href="#" title="Turquoise Theme">Turquoise Theme</a>
								</li>

							</ul>

						</div>

					</div>
					<div class="portlet-header ui-widget-header">Example Link<span class="ui-icon ui-icon-circle-arrow-a"></span></div>
					<!--<div class="portlet-content">

							<ul id="style-switcher" class="side-menu">

								<li>

									<a class="" id="" href="" title="">Example-1</a>

								</li>

								<li>

									<a class="" id="" href="" title="">Example-2</a>

								</li>

								<li>

									<a class="" id="" href="" title="">Example-3</a>

								</li>

								<li>

									<a class="" id="" href="" title="">Example-4</a>

								</li>

								

							</ul>

						</div>-->
<!--   
					<a class="fg-button btn ui-state-default full-link ui-corner-all" >

						<span class="ui-icon ui-state-zoomin"></span>

						Example Link

					</a>
-->
					

					<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">

						<div class="portlet-header ui-widget-header">Change layout width</div>

						<div class="portlet-content">

							<ul class="side-menu layout-options">

								<li>

									What width would you like the page to have ?<br /><br />

								</li>

								<li>

									<a href="javascript:void(0);" id="" title="Switch to 100% width layout">Switch to <b>100%</b> width</a>

								</li>

								<li>

									<a href="javascript:void(0);" id="layout90" title="Switch to 90% width layout">Switch to <b>90%</b> width</a>

								</li>

								<li>

									<a href="javascript:void(0);" id="layout75" title="Switch to 75% width layout">Switch to <b>75%</b> width</a>

								</li>

								<li>

									<a href="javascript:void(0);" id="layout980" title="Switch to 980px layout">Switch to <b>980px</b> width</a>

								</li>

								<li>

									<a href="javascript:void(0);" id="layout1280" title="Switch to 1280px layout">Switch to <b>1280px</b> width</a>

								</li>

								<li>

									<a href="javascript:void(0);" id="layout1400" title="Switch to 1400px layout">Switch to <b>1400px</b> width</a>

								</li>

								<li>

									<a href="javascript:void(0);" id="layout1600" title="Switch to 1600px layout">Switch to <b>1600px</b> width</a>

								</li>

							</ul>

						</div>

					</div>

					

					

					<div class="box ui-widget ui-widget-content ui-corner-all">

						<h3>Navigation</h3>

						<div class="content">

							<a class="btn ui-state-default full-link ui-corner-all" href="#">

								<span class="ui-icon ui-icon-mail-closed"></span>

								Dummy link

							</a>

							<a class="btn ui-state-default full-link ui-corner-all" href="#">

								<span class="ui-icon ui-icon-arrowreturnthick-1-n"></span>

								Dummy link

							</a>

							<a class="btn ui-state-default full-link ui-corner-all" href="#">

								<span class="ui-icon ui-icon-scissors"></span>

								Dummy link

							</a>

							<a class="btn ui-state-default full-link ui-corner-all" href="#">

								<span class="ui-icon ui-icon-signal-diag"></span>

								Dummy link

							</a>

							<a class="btn ui-state-default full-link ui-corner-all" href="#">

								<span class="ui-icon ui-icon-alert"></span>

								With icon and also quite large link

							</a>

						</div>

					</div>

					<div class="clear"></div>

					<div class="other-box yellow-box ui-corner-all ui-corner-all">

						<div class="cont tooltip ui-corner-all" title="Check out the sortable examples below !!">

							<h3>Sortable Section:</h3>

							<p>Below you will find a sortable area. Enjoy! Also a tooltip example. You can add tooltips for any html elements.</p>

						</div>

					</div>

					<div class="side_sort">

						<div class="box ui-widget ui-widget-content ui-corner-all">

							<h3>Sortable 1</h3>

							<div class="content">

								Lorem ipsum dolor sic amet dixit tu.

							</div>

						</div>

						<div class="box ui-widget ui-widget-content ui-corner-all">

							<h3>Sortable 2</h3>

							<div class="content">

								Lorem ipsum dolor sic amet dixit tu.

							</div>

						</div>

						<div class="box ui-widget ui-widget-content ui-corner-all">

							<h3>Datepicker</h3>

							<div class="content">

								Lorem ipsum dolor sic amet dixit tu.

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

		<div class="clear"></div>
			</div>

			<div class="clear"></div>

		</div>

	</div>

	<div class="clear"></div>

	<div id="footer">

		<a href="dashboard.php" title="Home">Home</a> | 

		<a href="#" title="Register">Register</a> | 

		<a href="#" title="Members Login">Members Login</a> | 

		<a href="#" title="About us">About us</a> | 

		<a href="#" title="Example link">Example link</a>

	</div>

<!-- Do not remove the copyright notice unless you have purchased a Commercial License from admintasia.com -->

	<div id="copyright">

		Powered by <a href="http://www.admintasia.com" title="Powerful admin UI template">Admintasia.com</a>

	</div>
	

<!-- Do not remove the copyright notice unless you have purchased a Commercial License from admintasia.com --></div>

</body>

</html>