<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"

"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">

<head> 

	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />

	<title>Admintasia 2.3</title>

	<script type="text/javascript" src="js/jquery-1.6.2.js"></script>

	<script type="text/javascript" src="js/ui/ui.core.js"></script>

	<script type="text/javascript" src="js/ui/ui.widget.js"></script>

	<script type="text/javascript" src="js/ui/ui.mouse.js"></script>

	<script type="text/javascript" src="js/superfish.js"></script>

	<script type="text/javascript" src="js/live_search.js"></script>

	<script type="text/javascript" src="js/tooltip.js"></script>

	<script type="text/javascript" src="js/cookie.js"></script>

	<script type="text/javascript" src="js/ui/ui.sortable.js"></script>

	<script type="text/javascript" src="js/ui/ui.draggable.js"></script>

	<script type="text/javascript" src="js/ui/ui.resizable.js"></script>

	<script type="text/javascript" src="js/ui/ui.position.js"></script>

	<script type="text/javascript" src="js/ui/ui.button.js"></script>

	<script type="text/javascript" src="js/ui/ui.dialog.js"></script>

	<script type="text/javascript" src="js/custom.js"></script>

	

	<link href="css/ui/ui.base.css" rel="stylesheet" media="all" />

	<link href="css/themes/black_rose/ui.css" rel="stylesheet" title="style" media="all" />



	<!--[if IE 6]>

	<link href="css/ie6.css" rel="stylesheet" media="all" />

	

	<script src="js/pngfix.js"></script>

	<script>

	  /* Fix IE6 Transparent PNG */

	  DD_belatedPNG.fix('.logo, ul#dashboard-buttons li a, .response-msg, #search-bar input');



	</script>

	<![endif]-->

</head>

<body>



	<div id="page_wrapper">

		<div id="page-header">

			<div id="page-header-wrapper">

				<div id="top">

					<a href="dashboard.php" class="logo" title="Admintasia 2.3">Admintasia 2.3</a>

					<div class="welcome">

						<span class="note">Welcome, <a href="#" title="Welcome, Horia Simon">Horia Simon</a></span>

						<a class="btn ui-state-default ui-corner-all" href="#">

							<span class="ui-icon ui-icon-wrench"></span>

							Settings

						</a>

						<a class="btn ui-state-default ui-corner-all" href="#">

							<span class="ui-icon ui-icon-person"></span>

							My account

						</a>

						<a class="btn ui-state-default ui-corner-all" href="#">

							<span class="ui-icon ui-icon-power"></span>

							Logout

						</a>						

					</div>

				</div>

				<ul id="navigation">

					<li>

						<a href="dashboard.php" class="sf-with-ul">Dashboard</a>

						<ul>

							<li><a href="dashboard.php">Administration</a></li>

							<li>

								<a href="forms.php">Forms</a>

								<ul>

									<li><a href="validate.php">Form validation</a></li>

									<li><a href="table_modal.php"><b>Add to table modal</b></a></li>

									<li><a href="editinplace.php"><b>Edit in Place</b></a></li>

									<li><a href="tinymce.php"><b>WYSIWYG Editor</b></a></li>

								</ul>

							</li>

							<li>

								<a href="tables.php">Tables</a>

								<ul>

									<li><a href="tables.php">Sortable Tables</a></li>

									<li><a href="flexigrid.php"><b>FlexiGrid</b></a></li>

								</ul>

							</li>

							<li>

								<a href="#">Widgets</a>

								<ul>

									<li><a href="accordion.php">Accordion</a></li>

									<li><a href="flexigrid.php"><b>FlexiGrid</b></a></li>

									<li><a href="editinplace.php"><b>Edit in Place</b></a></li>

									<li><a href="tinymce.php"><b>WYSIWYG Editor</b></a></li>

									<li><a href="charts.php"><b>Charts</b></a></li>

									<li><a href="tabs.php">Tabs</a></li>

									<li><a href="slider.php">Slider</a></li>

									<li><a href="datepicker.php">Datepicker</a></li>

									<li><a href="progress.php">Progress Bar</a></li>

									<li><a href="dialog.php">Dialogs and Modals</a></li>

									<li><a href="overlays.php">Overlays</a></li>

									<li><a href="photo_manager.php">Photo Manager</a></li>

									<li><a href="file_browser.php">File Browser</a></li>

								</ul>

							</li>

							<li><a href="msg.php">Response Messages</a></li>

							<li><a href="icons.php">Icons</a></li>

							<li><a href="index.php">Login Page</a></li>

							<li><a href="icons.php">Buttons and Elements</a></li>

						</ul>

					</li>

					<li>

						<a href="#" class="sf-with-ul">Unlimited Levels</a>

						<ul>

							<li>

								<a href="#" class="sf-with-ul">Menu item 1</a>

								<ul>

									<li><a href="#">Subitem 1</a></li>

									<li><a href="#">Subitem 2</a></li>

								</ul>

							</li>

							<li>

								<a href="#">Menu item 2</a>

							</li>

							<li>

								<a href="#">Menu item 3</a>

							</li>

							<li>

								<a href="#" class="sf-with-ul">Menu item 4</a>

								<ul>

									<li><a href="#">Subitem 1</a></li>

									<li>

										<a href="#" class="sf-with-ul">Subitem 2</a>

										<ul>

											<li><a href="#">Subitem 1</a></li>

											<li>

												<a href="#" class="sf-with-ul">Subitem 2</a>

												<ul>

													<li><a href="#">Subitem 1</a></li>

													<li>

														<a href="#">Subitem 2</a>

													</li>

												</ul>

											</li>

										</ul>

									</li>

								</ul>

							</li>

							<li>

								<a href="#" class="sf-with-ul">Menu item 5</a>

								<ul>

									<li><a href="#">Subitem 1</a></li>

									<li><a href="#">Subitem 2</a></li>

								</ul>

							</li>

							<li>

								<a href="#">Menu item 6</a>

							</li>

							<li>

								<a href="#">Menu item 7</a>

							</li>

						</ul>

					</li>

					<li><a href="gallery.php">Photo Gallery</a></li>

					<li>

						<a href="#" class="sf-with-ul">Layout Options</a>

						<ul>

							<li>

								<a href="three-columns-layout.php">Three columns</a>

							</li>

							<li>

								<a href="two-column-layout.php">Two columns</a>

							</li>

							<li>

								<a href="no-rounded.php">No rounded corners</a>

							</li>

							<li>

								<a href="content_boxes.php">Available content boxes</a>

							</li>

						</ul>

					</li>

					<li>

						<a href="#" class="sf-with-ul">Theme Options</a>

						<ul>

							<li>

								<a href="page_left_sidebar.php">Page with left sidebar</a>

							</li>

							<li>

								<a href="page_dynamic_sidebar.php">Page with dynamic sidebar</a>

							</li>

							<li>

								<a href="#">Avaiable Themes</a>

								<ul id="style-switcher">

									<li>

										<a class="set_theme" id="black_rose" href="#" title="Black Rose Theme">Black Rose Theme</a>

									</li>

									<li>

										<a class="set_theme" id="gray_standard" href="#" title="Gray Standard Theme">Gray Standard Theme</a>

									</li>

									<li>

										<a class="set_theme" id="gray_lightness" href="#" title="Gray Lightness Theme">Gray Lightness Theme</a>

									</li>

									<li>

										<a class="set_theme" id="apple_pie" href="#" title="Apple Pie Theme">Apple Pie Theme</a>

									</li>

									<li>

										<a class="set_theme" id="blueberry" href="#" title="Blueberry Theme">Blueberry Theme</a>

									</li>
									<li>

									<a class="set_theme" id="blue_sky" href="#" title="BLUE-light blue NAVIGATION BAR Theme">BlueSky Theme</a> 

								</li>

								<li>

									<a class="set_theme" id="salmon" href="#" title="White With Blue Theme">Salmon  Theme</a>

								</li>

								<li>

									<a class="set_theme" id="turquoise" href="#" title="White With Gray Theme">Turquoise Theme</a>

								</li>


								</ul>

							</li>

							<li>

								<a href="#"><i>Dummy Link</i></a>

							</li>

						</ul>

					</li>

					<li>

						<a href="#" class="sf-with-ul">Widgets</a>

						<ul>

							<li><a href="accordion.php">Accordion</a></li>

							<li><a href="flexigrid.php"><b>FlexiGrid</b></a></li>

							<li><a href="editinplace.php"><b>Edit in Place</b></a></li>

							<li><a href="tinymce.php"><b>WYSIWYG Editor</b></a></li>

							<li><a href="charts.php"><b>Charts</b></a></li>

							<li><a href="tabs.php">Tabs</a></li>

							<li><a href="slider.php">Slider</a></li>

							<li><a href="datepicker.php">Datepicker</a></li>

							<li><a href="progress.php">Progress Bar</a></li>

							<li><a href="dialog.php">Dialogs and Modals</a></li>

							<li><a href="overlays.php">Overlays</a></li>

							<li><a href="photo_manager.php">Photo Manager</a></li>

							<li><a href="file_browser.php">File Browser</a></li>

						</ul>

					</li>

				</ul>

				<div id="search-bar">

					<form method="post" action="http://www.google.com/">

						<input type="text" name="q" value="live search demo" />

					</form>

				</div>

			</div>

		</div>

  <script type="text/javascript" src="js/ui/ui.tabs.js"></script>
  <script type="text/javascript">

$(document).ready(function() {

	// Tabs

	$('#tabs, #tabs2, #tabs5').tabs();

});

</script>
  <div id="sub-nav">
    <div class="page-title"> 
      <h1>Dashboard</h1>
      <span><a href="#" title="Layout Options">Layout Options</a> > <a href="#" title="Two column layout">Two 
      column layout</a> > This is a breadcrumb example</span> </div>
    		<div id="top-buttons">

			<a id="dialog_link" class="btn ui-state-default ui-corner-all" href="#">

				<span class="ui-icon ui-icon-newwin"></span>

				Dialog Window

			</a>
     <ul class="drop-down">
				<li>

								<a class="btn ui-state-default ui-corner-all" id="drop" href="#drop_down">
					
									<span class="ui-icon ui-icon-carat-2-n-s"></span>
					
									DropDown Menu
					
								</a>
								
								<ul  class="drop-down-container box ui-widget ui-widget-content .ui-corner-tl .ui-corner-tr">
								        <li><a href="#" class="btn ui-state-default full-link ui-corner-all set_theme">DropDown Menu-1</a></li>

										<li><a href="#" class="btn ui-state-default full-link ui-corner-all set_theme">DropDown Menu-2</a></li>
					
										<li><a href="#" class="btn ui-state-default full-link ui-corner-all set_theme">DropDown Menu-3</a></li>
					
										<li><a href="#" class="btn ui-state-default full-link ui-corner-all set_theme">DropDown Menu-4</a></li>
					
										<li><a href="#" class="btn ui-state-default full-link ui-corner-all set_theme">DropDown Menu-5</a></li>
								</ul>
								</li>
								</ul>

			<div id="drop_down" class="hidden">

				<ul>

					<li><a href="#">Google</a></li>

					<li><a href="#">Yahoo</a></li>

					<li><a href="#">MSN</a></li>

					<li><a href="#">Ask</a></li>

					<li><a href="#">AOL</a></li>

				</ul>

			</div>

			<a id="modal_confirmation_link" class="btn ui-state-default ui-corner-all" href="#">

				<span class="ui-icon ui-icon-grip-dotted-horizontal"></span>

				Modal Confirmation

			</a>

		</div>

			<div id="dialog" title="Dialog Title">

				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>

			</div>

			<div id="modal_confirmation" title="An example modal title ?">

				<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>

			</div>

  </div>
  <div id="page-layout">
    <div id="page-content"> 
      <div id="page-content-wrapper"> 
        <div class="inner-page-title"> 
          <h2>Welcome to Admintasia 2.3 Live Demonstration</h2>
          <span>You can start building your next user interface with this powerful 
          UI framework !</span> </div>
        <div class="clear"></div>
        <div class="content-box"> 
          <div class="two-column"> 
            <div class="column"> 
              <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all"> 
                <div class="portlet-header ui-widget-header">Admintasia 2.3 is Here!!!<span class="ui-icon ui-icon-circle-arrow-s"></span></div>
                <div class="portlet-content"> 
                  <p> <a href="http://www.admintasia.com"><b>Admin Theme</b></a> 
                    | <b> <a href="http://www.admintasia.com/live-demo"><b>View 
                    Live Demonstration</b></a></b> | <b> <a href="http://www.admintasia.com/pricing"><b>Admintasia 
                    Licenses</b></a></b><br />
                    <br />
                    <a onclick="javascript:return EJEJC_lc(this);" title="Order Regular License" target="ej_ejc" href="https://www.e-junkie.com/ecom/gb.php?i=793722&amp;c=single&amp;cl=90436"><b>Regular 
                    License:</b></a> <strike>$84</strike> <b>$50</b>, <br />
                    <a onclick="javascript:return EJEJC_lc(this);" title="Order Developer License" target="ej_ejc" href="https://www.e-junkie.com/ecom/gb.php?i=770268&amp;c=single&amp;cl=90436"><b>Developer 
                    License: </b> </a><strike>$399</strike> <b>$99</b><a onclick="javascript:return EJEJC_lc(this);" title="Order Developer License" target="ej_ejc" href="https://www.e-junkie.com/ecom/gb.php?c=cart&amp;i=795480&amp;cl=90436&amp;ejc=2"><b><br />
                    Unlimited Developer License:</b></a> <strike>$699</strike> 
                    <b>$399</b><br />
                    <br />
                    All licenses come with <b>support access</b> and <b>updates</b>. 
                  </p>
                </div>
              </div>
            </div>
            <div class="column column-right"> 
              <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all"> 
                <div class="portlet-header ui-widget-header">Affiliate Program<span class="ui-icon ui-icon-circle-arrow-s"></span></div>
                <div class="portlet-content"> 
                  <p> Join our affiliates program and earn 51% of every sale. 
                    <br />
                    <br />
                    <b><a href="http://www.admintasia.com/affiliates/">Click here 
                    for more details about our affiliates program</a></b>. </p>
                </div>
              </div>
            </div>
          </div>
          <div class="clear"></div>
          <div class="response-msg inf ui-corner-all"> <span>Multiple Themes</span> 
            There are more themes available. To switch to another theme, click 
            on its link from the sidebar menu. </div>
        </div>
        <div class="clear"></div>
        <div class="inner-page-title"> 
          <h2>Example Three Column Layout</h2>
          <span>Lorem ipsum dolor sit amet, consectetuer adipiscing elit</span> 
        </div>
        <div class="three-column two-big-col"> 
          <div class="three-col-mid three-col-mid2"> 
            <div class="column col1"> 
              <div class="content-box content-box-header ui-corner-all"> 
                <div class="content-box-wrapper"> 
                  <h2>Multiple content boxes posibilities</h2>
                  <p>Admintasia contains a total of more than 180 icons. I only 
                    added a few for demonstration. </p>
                  <ul id="icons" class="ui-widget ui-helper-clearfix">
                    <li class="ui-state-default ui-corner-all" title=".ui-icon-star"><span class="ui-icon ui-icon-star"></span></li>
                    <li class="ui-state-default ui-corner-all" title=".ui-icon-link"><span class="ui-icon ui-icon-link"></span></li>
                    <li class="ui-state-default ui-corner-all" title=".ui-icon-cancel"><span class="ui-icon ui-icon-cancel"></span></li>
                    <li class="ui-state-default ui-corner-all" title=".ui-icon-plus"><span class="ui-icon ui-icon-plus"></span></li>
                    <li class="ui-state-default ui-corner-all" title=".ui-icon-plusthick"><span class="ui-icon ui-icon-plusthick"></span></li>
                    <li class="ui-state-default ui-corner-all" title=".ui-icon-minus"><span class="ui-icon ui-icon-minus"></span></li>
                    <li class="ui-state-default ui-corner-all" title=".ui-icon-minusthick"><span class="ui-icon ui-icon-minusthick"></span></li>
                    <li class="ui-state-default ui-corner-all" title=".ui-icon-close"><span class="ui-icon ui-icon-close"></span></li>
                    <li class="ui-state-default ui-corner-all" title=".ui-icon-script"><span class="ui-icon ui-icon-script"></span></li>
                    <li class="ui-state-default ui-corner-all" title=".ui-icon-alert"><span class="ui-icon ui-icon-alert"></span></li>
                  </ul>
                  <div class="clearfix"></div>
                  <i class="note">This is a note italic example ....</i> 
                  <div class="clearfix"></div>
                  <a href="#" class="btn ui-state-default ui-corner-all"> <span class="ui-icon ui-icon-grip-dotted-horizontal"></span> 
                  Button 1 </a> <a href="#" class="btn ui-state-default ui-corner-all"> 
                  <span class="ui-icon ui-icon ui-icon-gripsmall-diagonal-se"></span> 
                  Button 2 </a> <a href="#" class="btn ui-state-default ui-corner-all"> 
                  <span class="ui-icon ui-icon-grip-solid-horizontal"></span> 
                  Button 3 </a> <a href="#" class="btn ui-state-default ui-corner-all"> 
                  <span class="ui-icon ui-icon-grip-solid-vertical"></span> Button 
                  3 </a> 
                  <div class="clearfix"></div>
                  <p> 
                    <button class="ui-state-default float-left ui-corner-all ui-button" type="submit">Button 
                    Float Left</button>
                    <button class="ui-state-default float-right ui-corner-all ui-button" type="submit">Button 
                    Float Right</button>
                  </p>
                  <div class="clearfix"></div>
                  <p>Here is a pagination example:</p>
                  <ul class="pagination">
                    <li class="previous-off">&laquo;Previous</li>
                    <li class="active">1</li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">6</a></li>
                    <li><a href="#">7</a></li>
                    <li><a href="#">8</a></li>
                    <li><a href="#">9</a></li>
                    <li><a href="#">10</a></li>
                    <li class="next"><a href="#">Next &raquo;</a></li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
              </div>
            </div>
            <div class="column col2"> 
              <div class="content-box content-box-header"> 
                <div class="content-box-wrapper"> 
                  <h3>Title Example</h3>
                  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. 
                    Aenean commodo ligula eget dolor. Aenean massa.</p>
                  <p> Cum sociis natoque penatibus et magnis dis parturient montes, 
                    nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque 
                    eu, pretium quis, sem.</p>
                </div>
              </div>
            </div>
            <div class="column col3"> 
              <div class="content-box"> 
                <h4 class="ui-box-header ui-corner-all">Simple Table (title with 
                  H4 tag)</h4>
                <p>This is a simple table, but there is also an advanced sortable 
                  table available.</p>
                <i class="note">* Table example below: </i> 
                <div class="hastable"> 
                  <table cellspacing="0">
                    <thead>
                      <tr> 
                        <td><input type="checkbox" class="checkbox" value=""/></td>
                        <td>Name</td>
                        <td>Email</td>
                        <td>Options</td>
                      </tr>
                    </thead>
                    <tbody>
                      <tr> 
                        <td> <input type="checkbox" class="checkbox" value=""/> 
                        </td>
                        <td> John </td>
                        <td> Lorem . </td>
                        <td> Options </td>
                      </tr>
                      <tr class="alt"> 
                        <td> <input type="checkbox" class="checkbox" value=""/> 
                        </td>
                        <td> John </td>
                        <td> Lorem . </td>
                        <td> Options </td>
                      </tr>
                      <tr> 
                        <td> <input type="checkbox" class="checkbox" value=""/> 
                        </td>
                        <td> John </td>
                        <td> Lorem . </td>
                        <td> Options </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="clear"></div>
        <div class="inner-page-title"> 
          <h2>Dashboard Buttons</h2>
          <span>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, 
          consectetur, adipisci velit...</span> </div>
        <div id="dashboard-buttons"> 
          <ul>
            <li> <a href="#" class="Book_phones tooltip" title="Book phones"> 
              Book phones </a> </li>
            <li> <a href="#" class="Books tooltip" title="Books"> Books </a> </li>
            <li> <a href="#" class="Box_recycle tooltip" title="Box recycle"> 
              Box recycle </a> </li>
            <li> <a href="#" class="Books tooltip" title="Books"> Books </a> </li>
            <li> <a href="#" class="Box_content tooltip" title="Box content"> 
              Box content </a> </li>
            <li> <a href="#" class="Briefcase_files tooltip" title="Briefcase files"> 
              Briefcase files </a> </li>
            <li> <a href="#" class="Chart_4 tooltip" title="Chart 4"> Chart </a> 
            </li>
            <li> <a href="#" class="Clipboard_3 tooltip" title="Clipboard 3"> 
              Clipboard </a> </li>
            <li> <a href="#" class="Chart_5 tooltip" title="Chart 5"> Chart </a> 
            </li>
            <li> <a href="#" class="Glass tooltip" title="Glass"> Glass </a> </li>
            <li> <a href="#" class="Globe tooltip" title="Globe"> Globe </a> </li>
            <li> <a href="#" class="Mail_compose tooltip" title="Mail compose"> 
              Mail compose </a> </li>
            <li> <a href="#" class="Mail_open tooltip" title="Mail open"> Mail 
              open </a> </li>
            <li> <a href="#" class="Monitor tooltip" title="Monitor"> Monitor 
              </a> </li>
            <li> <a href="#" class="Star tooltip" title="Star"> Star </a> 
              <div class="clear"></div>
            </li>
          </ul>
          <div class="clear"></div>
        </div>
        <div class="inner-page-title"> 
          <h2>Sortable Portlets</h2>
          <span>The first example will show you a sortable portlets example</span> 
        </div>
        <div class="three-column sortable"> 
          <div class="three-col-mid"> 
            <div class="column col1"> 
              <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all"> 
                <div class="portlet-header ui-widget-header">Feeds<span class="ui-icon ui-icon-circle-arrow-s"></span></div>
                <div class="portlet-content"> 
                  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. 
                    Aenean commodo ligula eget dolor. Aenean massa.</p>
                  <p> Cum sociis natoque penatibus et magnis dis parturient montes, 
                    nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque 
                    eu, pretium quis, sem.</p>
                  <p> Nulla consequat massa quis enim. Donec pede justo, fringilla 
                    vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus 
                    ut, imperdiet a, venenatis vitae, justo.</p>
                  <p> Nullam dictum felis eu pede mollis pretium. Integer tincidunt. 
                    Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate 
                    eleifend tellus.</p>
                </div>
              </div>
              <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all"> 
                <div class="portlet-header ui-widget-header">News<span class="ui-icon ui-icon-circle-arrow-s"></span></div>
                <div class="portlet-content"> 
                  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. 
                    Aenean commodo ligula eget dolor. Aenean massa.</p>
                  <p> Cum sociis natoque penatibus et magnis dis parturient montes, 
                    nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque 
                    eu, pretium quis, sem.</p>
                  <p> Nulla consequat massa quis enim. Donec pede justo, fringilla 
                    vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus 
                    ut, imperdiet a, venenatis vitae, justo.</p>
                  <p> Nullam dictum felis eu pede mollis pretium. Integer tincidunt. 
                    Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate 
                    eleifend tellus.</p>
                </div>
              </div>
            </div>
            <div class="column col2"> 
              <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all"> 
                <div class="portlet-header ui-widget-header">Shopping<span class="ui-icon ui-icon-circle-arrow-s"></span></div>
                <div class="portlet-content"> 
                  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. 
                    Aenean commodo ligula eget dolor. Aenean massa.</p>
                  <p> Cum sociis natoque penatibus et magnis dis parturient montes, 
                    nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque 
                    eu, pretium quis, sem.</p>
                  <p> Nulla consequat massa quis enim. Donec pede justo, fringilla 
                    vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus 
                    ut, imperdiet a, venenatis vitae, justo.</p>
                  <p> Nullam dictum felis eu pede mollis pretium. Integer tincidunt. 
                    Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate 
                    eleifend tellus.</p>
                </div>
              </div>
            </div>
            <div class="column col3"> 
              <div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all"> 
                <div class="portlet-header ui-widget-header">Links<span class="ui-icon ui-icon-circle-arrow-s"></span></div>
                <div class="portlet-content"> 
                  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. 
                    Aenean commodo ligula eget dolor. Aenean massa.</p>
                  <p> Cum sociis natoque penatibus et magnis dis parturient montes, 
                    nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque 
                    eu, pretium quis, sem.</p>
                  <p> Nulla consequat massa quis enim. Donec pede justo, fringilla 
                    vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus 
                    ut, imperdiet a, venenatis vitae, justo.</p>
                  <p> Nullam dictum felis eu pede mollis pretium. Integer tincidunt. 
                    Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate 
                    eleifend tellus.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="clear"></div>
        <i class="note">To see more layout examples go to Layout Options Submenu 
        from the main top navigation bar.</i> 
        <div class="inner-page-title"> 
          <h3>Tabs example</h3>
        </div>
        <div id="tabs"> 
          <ul>
            <li><a href="#tabs-1">First</a></li>
            <li><a href="#tabs-2">Second</a></li>
            <li><a href="#tabs-3">Third</a></li>
          </ul>
          <div id="tabs-1">Lorem ipsum dolor sit amet, consectetur adipisicing 
            elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris 
            nisi ut aliquip ex ea commodo consequat.</div>
          <div id="tabs-2">Phasellus mattis tincidunt nibh. Cras orci urna, blandit 
            id, pretium vel, aliquet ornare, felis. Maecenas scelerisque sem non 
            nisl. Fusce sed lorem in enim dictum bibendum.</div>
          <div id="tabs-3">Nam dui erat, auctor a, dignissim quis, sollicitudin 
            eu, felis. Pellentesque nisi urna, interdum eget, sagittis et, consequat 
            vestibulum, lacus. Mauris porttitor ullamcorper augue.</div>
        </div>
        <div class="clear"></div>
        <div class="inner-page-title"> 
          <h3>Another Example</h3>
        </div>
        <div class="content-box content-box-header ui-corner-all"> 
          <div class="ui-state-default ui-corner-top ui-box-header"> <span class="ui-icon float-left ui-icon-signal"></span> 
            Content box example title </div>
          <div class="content-box-wrapper"> 
            <h3>Content Box Title Example 2 (with H3 tag)</h3>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean 
              commodo ligula eget dolor. Aenean massa.</p>
            <p> Cum sociis natoque penatibus et magnis dis parturient montes, 
              nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque 
              eu, pretium quis, sem.</p>
            <p> Nulla consequat massa quis enim. Donec pede justo, fringilla vel, 
              aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet 
              a, venenatis vitae, justo.</p>
            <p> Nullam dictum felis eu pede mollis pretium. Integer tincidunt. 
              Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend 
              tellus.</p>
          </div>
        </div>
        		<div id="sidebar">

			<div class="sidebar-content">

				<a id="close_sidebar" class="btn ui-state-default full-link ui-corner-all" >

					<span class="ui-icon ui-icon-circle-arrow-e"></span>

					Close Sidebar

				</a>

				<a id="open_sidebar" class="btn tooltip ui-state-default full-link icon-only ui-corner-all" title="Open Sidebar" ><span class="ui-icon ui-icon-circle-arrow-w"></span></a>

				<div class="hide_sidebar">

					<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">

						<div class="portlet-header ui-widget-header">Theme Switcher<span class="ui-icon ui-icon-circle-arrow-s"></span></div>

						<div class="portlet-content">

							<ul id="style-switcher" class="side-menu">

								<li>

									<a class="set_theme" id="black_rose" href="#" title="Black Rose Theme">Black Rose Theme</a>

								</li>

								<li>

									<a class="set_theme" id="gray_standard" href="#" title="Gray Standard Theme">Gray Standard Theme</a>

								</li>

								<li>

									<a class="set_theme" id="gray_lightness" href="#" title="Gray Lightness Theme">Gray Lightness Theme</a>

								</li>

								<li>

									<a class="set_theme" id="apple_pie" href="#" title="Apple Pie Theme">Apple Pie Theme</a>

								</li>

								<li>

									<a class="set_theme" id="blueberry" href="#" title="Blueberry Theme">Blueberry Theme</a>

								</li>
								<li>
									<a class="set_theme" id="blue_sky" href="#" title="BlueSky Theme">BlueSky Theme</a> 																	     							</li>							
								<li> 
									<a class="set_theme" id="salmon" href="#" title="Salmon  Theme">Salmon  Theme</a>
								</li>
								<li>
									<a class="set_theme" id="turquoise" href="#" title="Turquoise Theme">Turquoise Theme</a>
								</li>

							</ul>

						</div>

					</div>
					<div class="portlet-header ui-widget-header">Example Link<span class="ui-icon ui-icon-circle-arrow-a"></span></div>
					<!--<div class="portlet-content">

							<ul id="style-switcher" class="side-menu">

								<li>

									<a class="" id="" href="" title="">Example-1</a>

								</li>

								<li>

									<a class="" id="" href="" title="">Example-2</a>

								</li>

								<li>

									<a class="" id="" href="" title="">Example-3</a>

								</li>

								<li>

									<a class="" id="" href="" title="">Example-4</a>

								</li>

								

							</ul>

						</div>-->
<!--   
					<a class="fg-button btn ui-state-default full-link ui-corner-all" >

						<span class="ui-icon ui-state-zoomin"></span>

						Example Link

					</a>
-->
					

					<div class="portlet ui-widget ui-widget-content ui-helper-clearfix ui-corner-all">

						<div class="portlet-header ui-widget-header">Change layout width</div>

						<div class="portlet-content">

							<ul class="side-menu layout-options">

								<li>

									What width would you like the page to have ?<br /><br />

								</li>

								<li>

									<a href="javascript:void(0);" id="" title="Switch to 100% width layout">Switch to <b>100%</b> width</a>

								</li>

								<li>

									<a href="javascript:void(0);" id="layout90" title="Switch to 90% width layout">Switch to <b>90%</b> width</a>

								</li>

								<li>

									<a href="javascript:void(0);" id="layout75" title="Switch to 75% width layout">Switch to <b>75%</b> width</a>

								</li>

								<li>

									<a href="javascript:void(0);" id="layout980" title="Switch to 980px layout">Switch to <b>980px</b> width</a>

								</li>

								<li>

									<a href="javascript:void(0);" id="layout1280" title="Switch to 1280px layout">Switch to <b>1280px</b> width</a>

								</li>

								<li>

									<a href="javascript:void(0);" id="layout1400" title="Switch to 1400px layout">Switch to <b>1400px</b> width</a>

								</li>

								<li>

									<a href="javascript:void(0);" id="layout1600" title="Switch to 1600px layout">Switch to <b>1600px</b> width</a>

								</li>

							</ul>

						</div>

					</div>

					

					

					<div class="box ui-widget ui-widget-content ui-corner-all">

						<h3>Navigation</h3>

						<div class="content">

							<a class="btn ui-state-default full-link ui-corner-all" href="#">

								<span class="ui-icon ui-icon-mail-closed"></span>

								Dummy link

							</a>

							<a class="btn ui-state-default full-link ui-corner-all" href="#">

								<span class="ui-icon ui-icon-arrowreturnthick-1-n"></span>

								Dummy link

							</a>

							<a class="btn ui-state-default full-link ui-corner-all" href="#">

								<span class="ui-icon ui-icon-scissors"></span>

								Dummy link

							</a>

							<a class="btn ui-state-default full-link ui-corner-all" href="#">

								<span class="ui-icon ui-icon-signal-diag"></span>

								Dummy link

							</a>

							<a class="btn ui-state-default full-link ui-corner-all" href="#">

								<span class="ui-icon ui-icon-alert"></span>

								With icon and also quite large link

							</a>

						</div>

					</div>

					<div class="clear"></div>

					<div class="other-box yellow-box ui-corner-all ui-corner-all">

						<div class="cont tooltip ui-corner-all" title="Check out the sortable examples below !!">

							<h3>Sortable Section:</h3>

							<p>Below you will find a sortable area. Enjoy! Also a tooltip example. You can add tooltips for any html elements.</p>

						</div>

					</div>

					<div class="side_sort">

						<div class="box ui-widget ui-widget-content ui-corner-all">

							<h3>Sortable 1</h3>

							<div class="content">

								Lorem ipsum dolor sic amet dixit tu.

							</div>

						</div>

						<div class="box ui-widget ui-widget-content ui-corner-all">

							<h3>Sortable 2</h3>

							<div class="content">

								Lorem ipsum dolor sic amet dixit tu.

							</div>

						</div>

						<div class="box ui-widget ui-widget-content ui-corner-all">

							<h3>Datepicker</h3>

							<div class="content">

								Lorem ipsum dolor sic amet dixit tu.

							</div>

						</div>

					</div>

				</div>

			</div>

		</div>

		<div class="clear"></div>      </div>
      <div class="clear"></div>
    </div>
  </div>
  	<div class="clear"></div>

	<div id="footer">

		<a href="dashboard.php" title="Home">Home</a> | 

		<a href="#" title="Register">Register</a> | 

		<a href="#" title="Members Login">Members Login</a> | 

		<a href="#" title="About us">About us</a> | 

		<a href="#" title="Example link">Example link</a>

	</div>

<!-- Do not remove the copyright notice unless you have purchased a Commercial License from admintasia.com -->

	<div id="copyright">

		Powered by <a href="http://www.admintasia.com" title="Powerful admin UI template">Admintasia.com</a>

	</div>
	

<!-- Do not remove the copyright notice unless you have purchased a Commercial License from admintasia.com --></div>

</body>

</html>
